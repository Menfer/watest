package com.wa.test;

import java.io.IOException;
import java.io.InputStream;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.router.Route;
import com.wa.test.backend.CSVFilter;
import com.wa.test.backend.records.WARecord;

/**
 * The main view contains a button and a click listener.
 */
@Route("")
public class MainView extends VerticalLayout {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MainView() throws IOException {
		
		Label inputLabel = new Label("Please select your input file:");
		add(inputLabel);
		
		Label fileNameLabel = new Label();
        
		Grid<WARecord> recordGrid = new Grid<>();
        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);
        upload.setDropAllowed(false);
        add(upload);

        // On upload successful
        upload.addSucceededListener(event -> {
        	// This is done here as Upload mechanics in Vaadin 12 sometimes bug,
        	//and InputStream was closed before it could be used.
            InputStream is = buffer.getInputStream();
            CSVFilter filter;
			try {
				fileNameLabel.setText("Results for " + buffer.getFileName() + ":");
				add(fileNameLabel);
				filter = new CSVFilter(is);
	            recordGrid.setItems(filter.getEligibleRecords());
	            recordGrid.addColumn(WARecord::getId).setHeader("Id");
	            recordGrid.addColumn(WARecord::getVarList).setHeader(filter.getHeaders().toString());
	            recordGrid.addColumn(WARecord::getDecision).setHeader("Decision");
	            add(recordGrid);
			} catch (IOException e) {
				// Basic try-catch for the purposes of this demo
				e.printStackTrace();
			}
        });
        
        // Clean old data before starting a new upload
        upload.addStartedListener(event -> {
        	for (Grid.Column<WARecord> column : recordGrid.getColumns()) {
        		recordGrid.removeColumn(column);
        	}
        	remove(fileNameLabel);
        	remove(recordGrid);
        });
    }   
}
