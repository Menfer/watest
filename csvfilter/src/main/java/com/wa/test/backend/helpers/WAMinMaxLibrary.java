package com.wa.test.backend.helpers;

import java.util.ArrayList;

/**
 * Library for the MinMax helper classes
 * This is done as the number of Var columns is variable and we need an evaluator for each one
 * @author Tiago Mendonca Fernandes
 *
 */
public class WAMinMaxLibrary {
	
	private ArrayList<WAMinMax> library;
	
	/**
	 * Constructor for this library
	 * The library is created with default comparers and then each one is changed individually
	 * @param size number of comparers for the library (should be one per Var column)
	 */
	public WAMinMaxLibrary(int size) {
		this.library = new ArrayList<WAMinMax>(size);
		// Initialize library with default values
		for (int minMaxNumber = 0; minMaxNumber < size; minMaxNumber++) {
			this.library.add(new WAMinMax());
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<WAMinMax> getLibrary() {
		return this.library;
	}
	
	/**
	 * 
	 * @param index
	 * @param pair
	 */
	public void setWAMinMax(int index, WAMinMax pair) {
		this.library.set(index, pair);
	}
	
	/**
	 * 
	 * @param index
	 * @return
	 */
	public WAMinMax getWAMinMax(int index) {
		return this.library.get(index);
	}
}
