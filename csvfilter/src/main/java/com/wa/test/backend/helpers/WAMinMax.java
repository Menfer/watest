package com.wa.test.backend.helpers;

/**
 * Basic helper class for determining row eligibility for display
 * @author Tiago Mendonca Fernandes
 *
 */
public class WAMinMax {

	private int FMIN;
	private int FMAX;
	
	/**
	 * Constructor, sets default min and max values
	 */
	public WAMinMax() {
		this.setFMIN(Integer.MAX_VALUE);
		this.setFMAX(Integer.MIN_VALUE);
	}

	/**
	 * 
	 * @return
	 */
	public int getFMIN() {
		return FMIN;
	}

	/**
	 * 
	 * @param fMIN
	 */
	public void setFMIN(int fMIN) {
		FMIN = fMIN;
	}

	/**
	 * 
	 * @return
	 */
	public int getFMAX() {
		return FMAX;
	}

	/**
	 * 
	 * @param fMAX
	 */
	public void setFMAX(int fMAX) {
		FMAX = fMAX;
	}
	
	/**
	 * Method to determine if the argument value is within range of this comparer
	 * @param value what we want to check if is within this comparer's range
	 * @return true if FMIN <= value <= FMAX, false otherwise
	 */
	public boolean isInRange(int value) {
		if (value <= this.getFMAX() && value >= this.getFMIN())
			return true;
		return false;
	}
}
