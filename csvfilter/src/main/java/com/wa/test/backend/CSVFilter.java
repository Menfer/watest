package com.wa.test.backend;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.wa.test.backend.helpers.WAMinMaxLibrary;
import com.wa.test.backend.records.WARecord;
import com.wa.test.backend.records.WARecordLibrary;

/**
 * Main CSV filtering class
 * @author Tiago Mendonca Fernandes
 *
 */
public class CSVFilter {
	
	private WAMinMaxLibrary minMaxLibrary;
	private WARecordLibrary recordLibrary;
	private ArrayList<WARecord> eligibleRecordList;
	private int numberOfVars;
	private CSVParser parser;
	private Set<String> headers;
	
	/**
	 * Constructor for the filter class
	 * @param is uploaded file InputStream
	 * @throws IOException
	 */
	public CSVFilter(InputStream is) throws IOException {
		parser = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(new InputStreamReader(is));
		
		// Number of vars is header size minus id and decision fields
		numberOfVars = parser.getHeaderMap().size() - 2;
		headers = parser.getHeaderMap().keySet();
		headers.remove("Id");
		headers.remove("Decision");
		
		minMaxLibrary = new WAMinMaxLibrary(numberOfVars);
		recordLibrary = new WARecordLibrary();
		
		// Parse file to memory
		parseToMemory();

		// Now that we have a library of all the records in the CSV file, we can determine eligibility for
		//the ones with decision 0
		determineEligibility();
		
		// Now we build the eligible list to return to the client
		buildEligibleList();
		
	}
	
	/**
	 * Returns ArrayList with records that should be displayed
	 * @return
	 */
	public ArrayList<WARecord> getEligibleRecords() {
		return this.eligibleRecordList;
	}
	
	/**
	 * Parses CSV contents to memory and builds libraries
	 */
	public void parseToMemory() {
		Iterable<CSVRecord> itRecords = parser;
		// Iterate all records in CSV to memory, while determining MIN and MAX values for filtering
		// This is probably overkill for this demo, but since we already iterate the entire file for MIN and MAX values,
		// might just as well add it to objects in case we want to further work on them in the future.
		for (CSVRecord rec : itRecords) {
			WARecord record = new WARecord(Integer.parseInt(rec.get(0)), numberOfVars, Integer.parseInt(rec.get(numberOfVars+1)));
			for (int varNumber = 1; varNumber <= numberOfVars; varNumber++) {
				record.addVar(Integer.parseInt(rec.get(varNumber)));
				if(record.getDecision() == 1) {
					if(minMaxLibrary.getWAMinMax(varNumber-1).getFMAX() < Integer.parseInt(rec.get(varNumber))) {
						minMaxLibrary.getWAMinMax(varNumber-1).setFMAX(Integer.parseInt(rec.get(varNumber)));
					}
					if(minMaxLibrary.getWAMinMax(varNumber-1).getFMIN() > Integer.parseInt(rec.get(varNumber))) {
						minMaxLibrary.getWAMinMax(varNumber-1).setFMIN(Integer.parseInt(rec.get(varNumber)));
					}
				}
			}
			recordLibrary.addRecord(record);
		}
	}
	
	/**
	 * Goes through libraries and determined eligibility for each record
	 */
	public void determineEligibility() {
		for (WARecord record : recordLibrary.getLibrary()) {
			if(record.getDecision() == 0) {
				for (int index = 0; index < numberOfVars; index++) {
					if(minMaxLibrary.getWAMinMax(index).isInRange(record.getVar(index))) {
						record.setEligible(true);
						break;
					}
				}
			}
		}
	}
	
	/**
	 * Builds the ArrayList of eligible results
	 */
	public void buildEligibleList() {
		eligibleRecordList = new ArrayList<WARecord>();
		for (WARecord record : recordLibrary.getLibrary()) {
			if (record.isEligible())
				eligibleRecordList.add(record);
		}
		eligibleRecordList.trimToSize();
	}

	public Set<String> getHeaders() {
		return headers;
	}
	
}
