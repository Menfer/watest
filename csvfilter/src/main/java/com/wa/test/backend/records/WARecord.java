package com.wa.test.backend.records;

import java.util.ArrayList;

/**
 * Class that represents each line of the csv file
 * @author Tiago Mendonca Fernandes
 *
 */
public class WARecord {
	
	private int id;
	// As number of var columns can vary, opted to create an ArrayList
	private ArrayList<Integer> vars;
	private int decision;
	// This variable is used to determine if a row will be shown in the end
	private boolean eligible;
	
	/**
	 * Record constructor
	 * @param id row id
	 * @param numberOfVars number of Var columns
	 * @param decision decision value
	 */
	public WARecord(int id, int numberOfVars, int decision) {
		this.id = id;
		this.vars = new ArrayList<Integer>(numberOfVars);
		this.setDecision(decision);
		this.setEligible((decision == 1) ? true : false);
	}

	/**
	 * 
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @return
	 */
	public int getDecision() {
		return decision;
	}

	/**
	 * 
	 * @param decision
	 */
	public void setDecision(int decision) {
		this.decision = decision;
	}
	
	/**
	 * 
	 * @param var
	 */
	public void addVar(int var) {
		this.vars.add(var);
	}
	
	/**
	 * 
	 * @param index
	 * @return
	 */
	public int getVar(int index) {
		return this.vars.get(index);
	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<Integer> getVarList() {
		return vars;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEligible() {
		return eligible;
	}

	/**
	 * 
	 * @param eligible
	 */
	public void setEligible(boolean eligible) {
		this.eligible = eligible;
	}

}
