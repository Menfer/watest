package com.wa.test.backend.records;

import java.util.ArrayList;

/**
 * Class that represents all the records in the CSV file
 * @author Tiago Mendonca Fernandes
 *
 */
public class WARecordLibrary {
	
	private ArrayList<WARecord> library;
	
	/**
	 * Library constructor
	 */
	public WARecordLibrary() {
		this.library = new ArrayList<WARecord>();
	}
	
	/**
	 * Add new row to library
	 * @param rec
	 */
	public void addRecord(WARecord rec) {
		this.library.add(rec);
	}
	
	/**
	 * Get all rows
	 * @return rows as ArrayList
	 */
	public ArrayList<WARecord> getLibrary() {
		return this.library;
	}

}
